package com.emogi.gif.emogigiffrescoapp;

/**
 * Created by geetgobindsingh on 18/12/17.
 */

public class Constants {
    private Constants() {}

    public static final String EMOGI_GIF_PATH = "https://cdn.emogi.com/cxp/b8h/b8hbh-medium.gif";
}
