package com.emogi.gif.emogigiffrescoapp;

import android.graphics.drawable.Animatable;
import android.support.annotation.Nullable;

import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * Created by geetgobindsingh on 18/12/17.
 */

public class FrescoLoaderListener extends BaseControllerListener<ImageInfo> {

    FrescoLoadingCallBack frescoLoadingCallBack;

    public FrescoLoaderListener(FrescoLoadingCallBack frescoLoadingCallBack) {
        this.frescoLoadingCallBack = frescoLoadingCallBack;
    }

    @Override
    public void onFinalImageSet(String id, @Nullable ImageInfo imageInfo, @Nullable Animatable anim) {
        frescoLoadingCallBack.onSuccess();
    }

    @Override
    public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {
    }

    @Override
    public void onFailure(String id, Throwable throwable) {
        frescoLoadingCallBack.onFailure();
    }


    //interface as callback to info that image or gif downloaded successfully or not
    public interface FrescoLoadingCallBack {
        void onSuccess();

        void onFailure();
    }
}
