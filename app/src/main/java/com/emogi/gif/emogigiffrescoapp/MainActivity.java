package com.emogi.gif.emogigiffrescoapp;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private SimpleDraweeView mSimpleDraweeView1;
    private SimpleDraweeView mSimpleDraweeView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mSimpleDraweeView1 = findViewById(R.id.view1);
        mSimpleDraweeView2 = findViewById(R.id.view2);

        mSimpleDraweeView1.setController(getSimpleDraweeController(Constants.EMOGI_GIF_PATH));
        mSimpleDraweeView2.setController(getSimpleDraweeController(Constants.EMOGI_GIF_PATH));

    }

    private DraweeController getSimpleDraweeController(final String emogiGifPath) {
        final PipelineDraweeControllerBuilder pipelineDraweeControllerBuilder = Fresco.newDraweeControllerBuilder()
                .setAutoPlayAnimations(true)
                .setUri(Uri.parse(emogiGifPath));

        pipelineDraweeControllerBuilder.setControllerListener(new FrescoLoaderListener(new FrescoLoaderListener.FrescoLoadingCallBack() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Gif Loading Successful");
            }

            @Override
            public void onFailure() {
                Log.e(TAG, "Gif Loading Failed");
            }
        }));

        return pipelineDraweeControllerBuilder.build();
    }
}
