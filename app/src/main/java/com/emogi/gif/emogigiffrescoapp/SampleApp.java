package com.emogi.gif.emogigiffrescoapp;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by geetgobindsingh on 18/12/17.
 */

public class SampleApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
    }
}
